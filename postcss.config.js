const purgecss = require("@fullhuman/postcss-purgecss");

module.exports = {
  plugins: [
    require("cssnano"),
    require("autoprefixer") ,
    purgecss({
      content: ["./**/*.html"],
    }),
  ],
};
